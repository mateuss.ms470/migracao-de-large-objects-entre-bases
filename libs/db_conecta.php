<?
require_once('viewLog.php');

class DbConecta{

  /** Arquivo de configura��o das bases de dados. */
  const ARQUIVO_CONFIG = "db_config.ini";

 /**
  * Configura��o do Programa de Convers�o.
  * @var array
  */
  private $aConfigINI;

 /**
  * Conex�o de origem dos dados.
  * @var resource
  */
  private $rsConnOrigem;

 /**
  * Conex�o de destino dos dados.
  * @var resource
  */
  private $rsConnDestino;

 /**
  * Instacia��o da view de logs.
  * @var object
  */
  private $oViewLog;


 /**
  * Estabelece a conex�o com as bases de dados.
  *
  * @param void
  * @throws Exception
  * @return void
  */
  public function __construct(){
    try{
      $this->oViewLog = new IntegracaoLargeObjectsViewLog();
      $this->oViewLog->log("#\n");
      $this->oViewLog->log("INICIANDO CONEX�O\n");

      /** Inicializando Configura��o das bases */
      $this->aConfigINI = parse_ini_file(self::ARQUIVO_CONFIG);
      $this->conectaBaseOrigem();
      $this->conectaBaseDestino();
    }
    catch ( Exception $eException ) {
      $this->oViewLog->log('ERRO AO ESTABELECER A CONEX�O: '.$eException->getMessage()."\n");
      exit;
    }
  }


 /**
  * Estabelece a conex�o com o base de dados da origem.
  *
  * @param void
  * @throws Exception
  * @return void
  */
  private function conectaBaseOrigem(){
    /**  Configura��o base Origem*/ 
    $ConfigConexaoOrigem["host"]     = $this->aConfigINI["ConOrigem_host"];
    $ConfigConexaoOrigem["port"]     = $this->aConfigINI["ConOrigem_port"];
    $ConfigConexaoOrigem["dbname"]   = $this->aConfigINI["ConOrigem_dbname"];
    $ConfigConexaoOrigem["user"]     = $this->aConfigINI["ConOrigem_user"];
    $ConfigConexaoOrigem["password"] = $this->aConfigINI["ConOrigem_password"];

    $aValidacaoConfig = $ConfigConexaoOrigem;
    unset($aValidacaoConfig["password"]);
    if(in_array(null,$aValidacaoConfig)){
      throw new Exception('FALTANDO INFORMA��ES NO ARQUIVO DE CONFIGURA��O DA BASE '.$ConfigConexaoOrigem["dbname"]);
    }

    /** Conex�o com base de dados da Origem */
    $sDataSourceOrigem = "host={$ConfigConexaoOrigem["host"]} 
    dbname={$ConfigConexaoOrigem["dbname"]} 
    port={$ConfigConexaoOrigem["port"]} 
    user={$ConfigConexaoOrigem["user"]} 
    password={$ConfigConexaoOrigem["password"]}"; 

    if (! ($this->rsConnOrigem = @pg_connect($sDataSourceOrigem))) {
      throw new Exception('N�O FOI POSS�VEL SE CONECTAR COM A BASE '.$ConfigConexaoOrigem["dbname"]);
    }

    $this->oViewLog->log("CONEX�O COM A BASE ".$ConfigConexaoOrigem["dbname"]." OCORREU COM SUCESSO\n");
  }


 /**
  * Estabelece a conex�o com o base de dados do destino.
  *
  * @param void
  * @throws Exception
  * @return void
  */
  private function conectaBaseDestino(){
    /** Configura��o base Destino */ 
    $ConfigConexaoDestino["host"]        = $this->aConfigINI["ConDestino_host"];
    $ConfigConexaoDestino["port"]        = $this->aConfigINI["ConDestino_port"];
    $ConfigConexaoDestino["dbname"]      = $this->aConfigINI["ConDestino_dbname"];
    $ConfigConexaoDestino["user"]        = $this->aConfigINI["ConDestino_user"];
    $ConfigConexaoDestino["password"]    = $this->aConfigINI["ConDestino_password"];

    $aValidacaoConfig = $ConfigConexaoDestino;
    unset($aValidacaoConfig["password"]);
    if(in_array(null,$aValidacaoConfig)){
      throw new Exception('FALTANDO INFORMA��ES NO ARQUIVO DE CONFIGURA��O DA BASE '.$ConfigConexaoDestino["dbname"]);
    }

    /** Conex�o com base de destino */
    $sDataSourceDestino = "host={$ConfigConexaoDestino["host"]} 
    dbname={$ConfigConexaoDestino["dbname"]} 
    port={$ConfigConexaoDestino["port"]} 
    user={$ConfigConexaoDestino["user"]} 
    password={$ConfigConexaoDestino["password"]}";

    if (! ($this->rsConnDestino = pg_connect($sDataSourceDestino))) {
      throw new Exception('N�O FOI POSS�VEL SE CONECTAR COM A BASE '.$ConfigConexaoDestino["dbname"]);
    }
    $this->oViewLog->log("CONEX�O COM A BASE ".$ConfigConexaoDestino["dbname"]." OCORREU COM SUCESSO\n");
  }

 /**
  * Encerra a conex�o com as bases de dados.
  *
  * @throws Exception
  * @return void
  */
  public function encerrarConexao(){
    if(!(pg_close($this->rsConnOrigem)) or !(pg_close($this->rsConnDestino))){
      throw new Exception(pg_last_error($this->rsConnOrigem)."\n".pg_last_error($this->rsConnDestino));
    }
    $this->oViewLog->log("CONEX�O COM AS BASES DE DADOS ENCERRADA\n");

  }

 /**
  * Retorna a conex�o com o banco de dados de origem.
  *
  * @param void
  * @return resource
  */
  public function getConexaoOrigem() {
    return $this->rsConnOrigem;
  }


 /**
  * Retorna a conex�o com o banco de dados do destino.
  *
  * @param void
  * @return resource
  */
  public function getConexaoDestino() {
    return $this->rsConnDestino;
  }
}
?>