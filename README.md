# Migracao de Large Objects entre bases



## Para iniciar

Faça o clone do projeto.
git clone https://gitlab.com/mateuss.ms470/migracao-de-large-objects-entre-bases.git
ou
ssh git@gitlab.com:mateuss.ms470/migracao-de-large-objects-entre-bases.git

## Adicione o arquivo de configuração

```
cd migracao-de-large-objects-entre-bases
cp libs/db_config.ini.dist libs/db_config.ini

```
E preencha o arquivo com as informações das bases.

## Para Executar

Antes de executar o comando instale o php.
```
cd migracao-de-large-objects-entre-bases
php main.php
```
