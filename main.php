<?php
require_once('libs/db_conecta.php');
require_once('controller.php');

$oBdConecta = new DbConecta();

$oController = new IntegracaoLargeObjectsController($oBdConecta->getConexaoOrigem(),$oBdConecta->getConexaoDestino());

$oController->executar();

$oBdConecta->encerrarConexao();

?>