<?php

class IntegracaoLargeObjectsModel {


 /**
  * Realiza contagem dos Oids na base de dados.
  *
  * @param resource $rsConnDestino
  * @throws Exception
  * @return array
  */
  public function contarOids($rsConnDestino) {
    $sSql = 'SELECT count(oid) FROM pg_largeobject_metadata';
    $rsResultadoQueryOids = pg_query($rsConnDestino,$sSql);
    if(!($rsResultadoQueryOids)){
      throw new Exception(pg_last_error($rsConnDestino));
    }
    return pg_fetch_all($rsResultadoQueryOids);
  }

 /**
  * Realiza consulta de 1000 Oids da base de dados.
  *
  * @param resource $rsConnDestino
  * @throws Exception
  * @return array
  */
  public function consultarOids($rsConnDestino) {
    $sSql = 'SELECT oid FROM pg_largeobject_metadata limit 10000';
    $rsResultadoQueryOids = pg_query($rsConnDestino,$sSql);
    if(!($rsResultadoQueryOids)){
      throw new Exception(pg_last_error($rsConnDestino));
    }
    return pg_fetch_all($rsResultadoQueryOids);
  }

 /**
  * Exporta os Large Objects da base para o diret�rio tmp.
  *
  * @param resource $connOrigem
  * @param integer $iOid
  * @throws Exception
  * @return void
  */
  public function exportarLargeObject($connOrigem, $iOid) {
    if(!(pg_lo_export($connOrigem,intval($iOid),"tmp/arquivo_$iOid"))){
      throw new Exception(pg_last_error($connOrigem));
    }
  }

 /**
  * Importa os Large Objects do diret�rio tmp para a base de dados.
  *
  * @param resource $connDestino
  * @param integer $iOid
  * @param string $sArquivo
  * @throws Exception
  * @return void
  */
  public function inserirLargeObjects($connDestino, $iOid, $sArquivo) {
      if(!(pg_lo_import($connDestino,$sArquivo,$iOid))){
        throw new Exception(pg_last_error($connDestino));
      }
  }

 /**
  * Remove os Large Objects da base de dados.
  *
  * @param resource $connOrigem
  * @param integer $iOid
  * @throws Exception
  * @return void
  */
  public function removerLargeObjects($connOrigem, $iOid) {
    if(!(pg_lo_unlink($connOrigem,$iOid))){
      throw new Exception(pg_last_error($connOrigem));
    }
  }

  /**
  * Executa o vacuum nas tabelas da base de origem.
  *
  * @param resource $connOrigem
  * @throws Exception
  * @return void
  */
  public function vacuumLargeObjects($connOrigem){
    
    if(!(pg_query($connOrigem, "vacuum full pg_largeobject"))){
      throw new Exception(pg_last_error($connOrigem));
    }
    if(!(pg_query($connOrigem, "vacuum full pg_largeobject_metadata"))){
      throw new Exception(pg_last_error($connOrigem));
    }
  }

  /**
  * Executa o reindex nas tabelas da base de origem.
  *
  * @param resource $connOrigem
  * @throws Exception
  * @return void
  */
  public function reindexLargeObjects($connOrigem){
    
    if(!(pg_query($connOrigem, "reindex table pg_largeobject"))){
      throw new Exception(pg_last_error($connOrigem));
    }
    if(!(pg_query($connOrigem, "reindex table pg_largeobject_metadata"))){
      throw new Exception(pg_last_error($connOrigem));
    }
  }

  /**
  * Inicia transa��o.
  *
  * @param resource $conn
  * @throws Exception
  * @return void
  */
  public function beginTransaction($conn){
    if(!(pg_query($conn, "BEGIN"))){
      throw new Exception(pg_last_error($conn));
    }
  }

  /**
  * Finaliza transa��o.
  *
  * @param resource $conn
  * @throws Exception
  * @return void
  */
  public function commitTransaction($conn){
    if(!(pg_query($conn, "COMMIT"))){
      throw new Exception(pg_last_error($conn));
    }
  }
}
?>