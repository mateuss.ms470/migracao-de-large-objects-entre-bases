<?php
require_once('model.php');
require_once('viewLog.php');
ini_set('display_errors',1);
ini_set('display_startup_erros',1);
error_reporting(E_ALL);
class IntegracaoLargeObjectsController {

   /**
    * Instancia��o do model.
    * @var object
    */
    private $oModel;

   /**
    * Instancia��o da view de logs.
    * @var object
    */
    private $oViewLog;

   /**
    * N�mero de Oids da Base de origem.
    * @var integer
    */
    private $iNumeroOidsGeral;

   /**
    * Porcetagem geral da migra��o.
    * @var float
    */
    private $fPorcetagemLote;

   /**
    * Lista com pacote os large objects da Base de origem.
    * @var array
    */
    private $aLoteOids;

   /**
    * Conex�o de origem dos dados 
    * @var resource
    */
    private $rsConnOrigem;

   /**
    * Conex�o de destino dos dados 
    * @var resource
    */
    private $rsConnDestino;

   /**
    * Inicializa a migra��o com o model e as conex�es das bases de dados.
    *
    * @param resource $connOrigem
    * @param resource $connDestino
    * @throws Exception
    * @return void
    */
    public function __construct($connOrigem,$connDestino){
            $this->oModel = new IntegracaoLargeObjectsModel();
            $this->oViewLog = new IntegracaoLargeObjectsViewLog();
            $this->rsConnOrigem = $connOrigem;
            $this->rsConnDestino = $connDestino;
    }

   /**
    * Executa os procedimentos necess�rios � execu��o.  
    *
    * @throws Exception
    * @return void
    */
    public function executar(){
        try{
            $this->oViewLog->log("INICIANDO MIGRA��O\n");

            $this->iNumeroOidsGeral = $this->oModel->contarOids($this->rsConnOrigem)[0]['count'];
            $iNumeroLote = 1;

            while($this->verificarOidsBaseOrigem()){
                $this->carregarOidsBaseOrigem($iNumeroLote);

                $this->oViewLog->log("INICIANDO MIGRA��O DO LOTE $iNumeroLote\n");
                $this->migrarDados($iNumeroLote);

                $iNumeroLote++;      
            }
            $this->oViewLog->log("VACUUM ORIGEM\n");
            $this->oModel->vacuumLargeObjects($this->rsConnOrigem);

            $this->oViewLog->log("MIGRA��O ENCERRADA\n");
        } 
        catch ( Exception $eException ) {
            $this->oViewLog->log('ERRO AO EXECUTAR A MIGRA��O: '.$eException->getMessage()."\n");
            exit;
        }
    }

   /**
    * Verifica se existem Oids na base de Origem.  
    *
    * @throws Exception
    * @return void
    */
    private function verificarOidsBaseOrigem(){
        $iNumeroOids = $this->oModel->contarOids($this->rsConnOrigem)[0]['count'];
        if($iNumeroOids == 0){
            return false;
        }
        return true;
    }


   /**
    * Carrega os Oids da base de Origem.  
    *
    * @throws Exception
    * @return void
    */
    private function carregarOidsBaseOrigem($iNumeroLote){
        $this->aLoteOids = [];
        $aSqlLoteOids = $this->oModel->consultarOids($this->rsConnOrigem);
        foreach ($aSqlLoteOids as $aOid) {
            array_push($this->aLoteOids,$aOid['oid']);
        }
        if ($this->aLoteOids == []){
            throw new Exception('N�O EXISTEM LARGE OBJECTS NA BASE DE ORIGEM');
        }
        $this->oViewLog->log("LOTE ".$iNumeroLote." CARREGADO DA BASE ORIGEM COM SUCESSO\n");
    }


   /**
    * Realizar a migra��o dos dados
    * @throws Exception
    * @return void
    */
    private function migrarDados($iNumeroLote){
        $iIndice = 1;
        foreach ($this->aLoteOids as $iOid){
            $this->oModel->beginTransaction($this->rsConnOrigem);
            $this->oModel->beginTransaction($this->rsConnDestino);
            
            $this->oModel->exportarLargeObject($this->rsConnOrigem,$iOid);

            chdir( 'tmp' );
            $aArquivos = glob("{*}", GLOB_BRACE);
    
            $this->oModel->inserirLargeObjects($this->rsConnDestino,$iOid,$aArquivos[0]);
            

            unlink($aArquivos[0]);
            chdir( '../' );

            $this->oModel->removerLargeObjects($this->rsConnOrigem,$iOid);

            $this->oModel->commitTransaction($this->rsConnOrigem);
            $this->oModel->commitTransaction($this->rsConnDestino);

            $this->fPorcetagemLote = number_format($iIndice/(count($this->aLoteOids)/100),2);
            $iIndice++;

            $this->oViewLog->log("MIGRA��O DO LOTE $iNumeroLote EST� EM ".$this->fPorcetagemLote."%\n");

        }
    }

   
}
?>