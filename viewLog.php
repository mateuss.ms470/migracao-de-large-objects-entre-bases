<?php
class IntegracaoLargeObjectsViewLog{

  /** Arquivo de log da migra��o. */
  const ARQUIVO_LOG = "log/MIGRACAO_LARGEOBJECTS.log";

   /**
    * Imprimi os log no arquivo e na tela.
    *
    * Tipos: 0 = Saida Tela e Arquivo
    *        1 = Saida Somente Tela
    *        2 = Saida Somente Arquivo
    *
    * @param string $sLog
    * @param integer $iTipo
    * @param boolean $lLogDataHora
    * @param boolean $lQuebraAntes
    * @throws Exception
    * @return void
    */
    public function log($sLog="", $iTipo=0, $lLogDataHora=true, $lQuebraAntes=true) {
        if ($sLog == "#\n"){
            $iTipo = 2;
        }

        if(empty($sLog)){
            echo "Erro no Log: Mensagem de log vazia!";
            exit;
        }

        if(empty(self::ARQUIVO_LOG)){
            echo "Erro no Log: Arquivo de log n�o existe!";
            exit;
        }

        $aDataHora = getdate();
        $sQuebraAntes = $lQuebraAntes?"\n":"";
    
        if($lLogDataHora) {
            $sOutputLog = sprintf("%s[%02d/%02d/%04d %02d:%02d:%02d] %s", $sQuebraAntes,
                                   $aDataHora["mday"], $aDataHora["mon"], $aDataHora["year"],
                                   $aDataHora["hours"], $aDataHora["minutes"], $aDataHora["seconds"],
                                   $sLog);
        } else {
            $sOutputLog = sprintf("%s%s", $sQuebraAntes, $sLog);
        }
    
        // Se habilitado saida na tela.
        if($iTipo==0 or $iTipo==1) {
            echo $sOutputLog;
        }
        
        // Se habilitado saida para arquivo.
        if($iTipo==0 or $iTipo==2) {
            if(!(empty(self::ARQUIVO_LOG))) {
                $fd=fopen(self::ARQUIVO_LOG, "a+");
                if($fd) { 
                    fwrite($fd, $sOutputLog);
                    fclose($fd);
                }
            }
        }
 
    }
}
?>